# Usr/bin/env python
# Python Triks to optimize the loc
# Unpacking Variables
a, b, c = 1, 2, 3
print a
print b 
print c

print "-"*40
a, b, c = [1, 2, 3]
print a
print b
print c
print "-"*40

a, b, c = (1, 2, 3)
print a
print b
print c
print "-"*40

# Unpacking for swapping variables
a, b = 1, 2
print "-"*40
print "Before Swapping"
print a
print b

b, a = a, b
print "-"*40
print "After Swapping"
print a
print b