# Usr/bin/env python
# Using Strings in python
string = "Hello"
# Find the length of the string using builtin functions
print len(string)

# Reverse a string without loops
print string[::-1]