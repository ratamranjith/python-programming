# Usr/bin/env python2.7
fruits = { 'Apple': 'Sweet one, Red Color',
          'Orange': 'Ripe one, Orange COlor',
          'Banana': 'Sweet one, Yellow Color',
          'Grapes': 'Ripe one, Green Color'}# 1 - Print Dictionary

# Print Dictionary
print fruits

# 1 - Print Dictionary keys using loops
# For Loops - Type 1
for keys in fruits:
    print keys

print "*" * 40
# 2 - Print Dictionary keys using Builtin Functions
# It will be stored in an array manner
print fruits.keys()
print "*" * 40

# 3 - Print Dictionary keys by combing both
for keys in fruits.keys():
    print keys
print "*" * 40

# Similiar way we can use for values too
# 1 . Using loops
for keys in fruits:
    print  fruits[keys]
print "*" * 40

# 2. Using Builtin Functions
print fruits.values()
print "*" * 40