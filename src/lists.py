# Usr/bin/env python

# Negative indexing in Lists
a = [1,2,3,4,5,6,7,8,9,0]

# Normal Indexing
print a[3]

# Negative Indexing
print a[-3] # It will fetch the value from last index level

# Slices in List
print a[2:4]

# Slices in list with Negative indexing
print a[-3: -1]

# Lists with steps
# print odd numbers in list
# we can use step in slices
print a[::2]
